def func1(cel):
    kel = cel + 273
    return int(kel)


def func2(kel):
    cel = kel - 273
    return int(cel)


def func3(fah):
    cel = 5.0 * (fah - 32) / 9
    return int(cel)


def func4(cel):
    fah = (9 / 5.0 * cel) + 32
    return int(fah)


def func5(fah):
    kel = 5.0 * (fah - 32) / 9 + 273
    return int(kel)


def func6(kel):
    fah = (9 / 5.0 * (kel - 273)) + 32
    return int(fah)


def preobrazovanie(value, first_name, second_name):

    try:
        value = int(value)
    except Exception:
        return 'Value must be integer!'

    if first_name == second_name:
        return 'translation into the same soc is not possible'
    if first_name != 'cel' and first_name != 'kel' and first_name != 'fah' and second_name != 'cel' and second_name != 'kel' and second_name != 'fah':
        return 'have not so soc'


    if first_name == 'fah':
        if second_name == 'cel':
            result = func3(value)
        if second_name == 'kel':
            result = func5(value)
    if first_name == 'cel':
        if second_name == 'fah':
            result = func4(value)
        if second_name == 'kel':
            result = func1(value)
    if first_name == 'kel':
        if second_name == 'fah':
            result = func6(value)
        if second_name == 'cel':
            result = func2(value)

    with open('test.txt', 'a') as the_file:
        the_file.write(f'{value} {first_name} = {result} {second_name}\n')

    return result
